import React from 'react'
//import { Table } from 'antd'


const UserTable = (props) => (
    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>mail</th>
                <th>phone</th>
            </tr>
        </thead>
        <tbody>
            {props.users.length > 0 ? (
                props.users.map((user) => (
                    <tr key={user.id}>
                        <td>{user.name}</td>
                        <td>{user.mail}</td>
                        <td>{user.id}</td>
                        <td>
                            <button
                                onClick={() => props.deleteuser(user.id)}
                                className="button muted-button">Delete</button>
                        </td>
                    </tr>
                ))
            ) : (
                    <tr>
                        <td colSpan={3}>No users</td>
                    </tr>
                )}
        </tbody>
    </table>
)

export default UserTable;