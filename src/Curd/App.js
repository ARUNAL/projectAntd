import React, { useState, useRef, useEffect } from 'react'

import UserTable from './UserTable'
const LOCAL_STORAGE_KEY = 'userApp.users'

function App() {
    const [users, setUsers] = useState([{}])
    const userNameRef = useRef()
    const userMailRef = useRef()
    const userPhoneRef = useRef()

    useEffect(() => {
        const storedusers = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY))
        if (storedusers) setUsers(storedusers)

    }, [])

    useEffect(() => {
        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(users))
    }, [users])
    function handleAdduser(e) {
        const name = userNameRef.current.value
        const mail = userMailRef.current.value
        const phone = userPhoneRef.current.value
        if (name === '' || mail === '' || phone === '') return
        setUsers(prevusers => {
            return [...prevusers, { id: phone, name: name, mail: mail, complete: false }]
        })


    }
    const deleteuser = (id) => {
        setUsers(users.filter((user) => user.id !== id))
    }

    return (
        <div>

            <p>Name:<input ref={userNameRef} type="text" /></p>
            <p>Email:<input ref={userMailRef} type="text" /></p>
            <p>Phone:<input ref={userPhoneRef} type="number" /></p>
            <span><textarea type="text" /></span>
            <button onClick={handleAdduser}>Add user</button>
            <UserTable users={users} deleteuser={deleteuser} />

        </div>

    )
}

export default App

